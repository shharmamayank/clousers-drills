
function cacheFunction(callback) {
    let cache = {}
    return (arguments)=>{
        if (cache[arguments]) {
            return cache[arguments]
        } else {
            cache[arguments] = callback(arguments)
            return cache[arguments]
        }
    }

}

module.exports = cacheFunction