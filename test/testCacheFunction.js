let cacheFunction = require("../cacheFunction")
function callback(arguments) {
    return arguments * 2;
}
const cache = cacheFunction(callback)
for (let index = 0; index < 5; index++) {
    console.log(cache(index))
}
// console.log(cacheFunction);